#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# © Clément Joly leowzukw@vmail.me, (2015)
#
# leowzukw@vmail.me
#
# Ce logiciel est un programme informatique servant à créer des graphiques de
# bandes passantes.
#
# Ce logiciel est régi par la licence CeCILL-B soumise au droit
# français et
# respectant les principes de diffusion des logiciels libres. Vous pouvez
# utiliser, modifier et/ou redistribuer ce programme sous les conditions
# de la licence CeCILL-B telle que diffusée par le CEA, le CNRS
# et l'INRIA
# sur le site "http://www.cecill.info".
#
# En contrepartie de l'accessibilité au code source et des droits de copie,
# de modification et de redistribution accordés par cette licence, il n'est
# offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
# seule une responsabilité restreinte pèse sur l'auteur du programme,  le
# titulaire des droits patrimoniaux et les concédants successifs.
#
# A cet égard  l'attention de l'utilisateur est attirée sur les risques
# associés au chargement,  à l'utilisation,  à la modification et/ou au
# développement et à la reproduction du logiciel par l'utilisateur étant
# donné sa spécificité de logiciel libre, qui peut le rendre complexe à
# manipuler et qui le réserve donc à des développeurs et des professionnels
# avertis possédant  des  connaissances  informatiques approfondies.  Les
# utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
# logiciel à leurs besoins dans des conditions permettant d'assurer la
# sécurité de leurs systèmes et ou de leurs données et, plus généralement,
# à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
#
# Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
# pris connaissance de la licence CeCILL-B, et que vous en avez
# accepté les
# termes.
#

def getData(fingerprints):
    """getData fetch data from https://onionoo.torproject.org.
    See https://onionoo.torproject.org/protocol.html"""
    import urllib
    import urllib.request

    # Const
    urlApi='https://onionoo.torproject.org/bandwidth?fingerprint='
    # For the result
    data=[]

    for fp in fingerprints:
        url = urlApi + fp
        # XXX Debug
        print("url: ", url)
        d=urllib.request.urlopen(url).read().decode('utf-8')
        data.append(d)

    return data

def treatData(dataSet):
    """Read JSON data"""
    import json

    # Get usefull datas
    usefullData=[]

    for jsonData in dataSet:
        bandwidth=json.loads(jsonData)['relays'][0]['write_history']['3_months']['values']
        usefullData.append(bandwidth)

    return usefullData

def createGraph(bandwidthUsed, keys):
    """createGraph creates the graphs"""
    # TODO to be improved
    import pygal

    pie_chart = pygal.Pie(inner_radius=.4)
    pie_chart.title = 'TODO'

    for fingerprint, vals in zip(keys, bandwidthUsed):
        # XXX DEBUG
        print('vals: ', vals)

        pie_chart.add(fingerprint, vals)

    pie_chart.render_to_file('donuts.svg')

def main():
    fingerprints=['EFAE44728264982224445E96214C15F9075DEE1D', # Marcuse 1
                  'C656B41AEFB40A141967EBF49D6E69603C9B4A11', # Marcuse 2
                  '9BA84E8C90083676F86C7427C8D105925F13716C', # Ekumen
                  '578E007E5E4535FBFEF7758D8587B07B4C8C5D06', # Marylou 1
                  '90FD830C357A5109AB3C505287713F1AC811174C', # Marylou 1
                 ]

    bandwidths = treatData(getData(fingerprints))
    createGraph(bandwidths, fingerprints)

    return True

main()
